// $(window).scroll(function () {
//     // console.log('Yihuy!!');

//     if ($('.navbar').scrollTop() <= 20) {
//         console.log('Yihuy!!');
//     } else if ($('.navbar').scrollTop() >= 21) {
//         console.log('Yieuh!!');
//     }
// });

// var distance = $('.navbar').offset().top,
//     $window = $(window);

// $window.scroll(function () {
//     if ($window.scrollTop() < distance) {
//         $('.navbar').addClass('_home')
//     }
//     else if ($window.scrollTop() > distance) {
//         $('.navbar').removeClass('_home')
//     }
// });

// $window.ready(function () {
//     if ($window.scrollTop() < distance) {
//         $('.navbar').addClass('_home')
//     }
//     else if ($window.scrollTop() > distance) {
//         $('.navbar').removeClass('_home')
//     }
// });

$('.drawer-btn').click(function () {
    $('.navbar').addClass('_active');
    $('body').addClass('no-overflow');
});

$('.drawer-btn-close').click(function () {
    $('.navbar').removeClass('_active');
    $('body').removeClass('no-overflow');
});


$('.split-input').keyup(function () {
    if ($(this).val().length == $(this).attr("maxlength")) {
        $(this).next().focus();
    }
});

function numberWithCommas(number) {
    var parts = number.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return parts.join(".");
}

$(".ns").each(function () {
    var num = $(this).text();
    var commaNum = numberWithCommas(num);
    $(this).text(commaNum);
});

$('._reservasi [type="checkbox"]').change(function () {
    if ($(this).is(':checked')) {
        $(this).parent('._reservasi').addClass('_checked');
    }
    else {
        $(this).parent('._reservasi').removeClass('_checked');
    };
});

$('.profile-drawer, .profile-menu').click(function () {
    $('.profile-menu').toggleClass('_show');
});

$(document).scroll(function () {
    if ($(this).scrollTop() >= $('.main-header').offset().top + 1) {
        $(".navbar--home").addClass("_scrolled");
    } else {
        $(".navbar--home").removeClass("_scrolled");
    }
});

// Smooth Link
$('.primary-card._v2').click(function () {
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 20
    }, 720);
    return false;
});